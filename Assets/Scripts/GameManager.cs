using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private GameManager _inst;
    public static GameManager Instance {get{return Instance;}}

    public GameObject Player;
    public UIController UIController;
    public static int _score;
    public int enemyNum;

    private void Awake()
    {
        if (_inst == null)
        {
            DontDestroyOnLoad(gameObject);
            _inst = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        _score = 0;
        enemyNum = 0;

        Player = GameObject.Find("Player");
        UIController = GameObject.Find("Canvas").GetComponent<UIController>();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void addScore()
    {
        _score = +5;
    }

}
