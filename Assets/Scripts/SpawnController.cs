using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
    [SerializeField]
    private GameObject enemy;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("CreateEnemy", 1, 5);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void CreateEnemy()
    {
        int rnd = Random.Range(0, 3);
        if(rnd == 0)
        {
            Instantiate(enemy, new Vector3(-10, Random.Range(-5, 5), 0), Quaternion.identity);
        }
        else if (rnd == 1)
        {
            Instantiate(enemy, new Vector3(10, Random.Range(-5, 5), 0), Quaternion.identity);

        }
        else if (rnd == 2)
        {
            Instantiate(enemy, new Vector3(Random.Range(-10, 10), -6, 0), Quaternion.identity);

        }
        else if (rnd == 3)
        {
            Instantiate(enemy, new Vector3(Random.Range(-10, 10), 6, 0), Quaternion.identity);
        }
    }
}
