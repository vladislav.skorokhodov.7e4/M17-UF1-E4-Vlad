using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCollitionController : MonoBehaviour
{
    //Collition controller with enemy and objects
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            GameObject pl = collision.gameObject;
            PlayerStats.lifes--;
            Debug.Log("DEAD");
        }
        Destroy(gameObject);
        GameManager._score = +5;
    }

    void OnMouseDown()
    {
        Destroy(this.gameObject);
        GameManager._score =+ 5;
    }
}
