using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyMoveController : MonoBehaviour
{
    public GameObject plater;
    public Transform target;

    public float speed = 1f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        target = GameObject.FindGameObjectsWithTag("Player")[0].transform;
        transform.position = Vector3.MoveTowards(transform.position, target.transform.position, speed * Time.deltaTime);
        transform.Rotate(0, 0, 2000 * Time.deltaTime);
    }
}
